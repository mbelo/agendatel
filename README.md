# Trabalho de Banco de Dados AgendaTel

## Contexto

A aplicação *AgendaTel* é uma aplicação Java usando a biblioteca de janelas
gráficas ao usuário (*GUI*) chamada *Swing*. Basicamente a aplicação é um *CRUD*
para entradas em uma agenda telefônica com nome e telefone.

## Preparação

1. Tenha o repositório da disciplina clonado em seu máquina

> Assista aos vídeos instrucionais sobre GIT na página do curso

2. realizar download do fonte inicial para uma pasta no seu repositório da disciplina

- [Download do Fonte do AgendaTel no BitBucket](https://bitbucket.org/mbelo/agendatel/downloads/)

- Clicar no link 'Download repository'

3. Baixar uma IDE

Use uma IDE de sua preferência.

Dica: se você quiser alterar a interface com o usuário, no NetBeans há um editor visual de telas Swing's muito bom.

[Link NetBeans](https://netbeans.apache.org)

4. Abrir o projeto e rodar

Teste a aplicação usando serialização em disco.

Explore o código da aplicação: a aplicação usa *Swing* para criação da interface com o usuário.

## O Trabalho

Após sua análise do código, você perceberá que os registros são salvos num objeto serializável que é persistido num arquivo em disco (`agenda.ser`).

Mude a aplicação para que ele persista os registros num banco de dados
*MySQL*.

> Dica: em *Windows*, use um software como WampServer (ou VertrigoServ). Ele
cria uma instalação local do *MySQL* de forma simples para começar a usar.
Teste sua conexão antes de tentar codificar a solução.